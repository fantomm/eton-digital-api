<?php

namespace AppBundle\Helper;


class ValidationHelper
{

    /**
     * Validate entity using class Asserts
     * @param $entity
     * @param $container
     * @return string   Error messages || empty string
     */
    public static function validateEntity($entity, $container)
    {
        $validator = $container->get('validator');
        $errors = $validator->validate($entity);

        $errorsResponse = '';
        if (count($errors) > 0)
        {
            foreach ($errors as $error) {
                $errorsResponse .= $error->getMessage().' ';
            }
        }
        return $errorsResponse;
    }


    /**
     * Check if provided data is JSON format
     * @param $string
     * @return bool
     */
    public static function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }


    /**
     * Check if passwords match
     * @param $user
     * @return mixed
     */
    public static function checkPasswordConfirmation($user)
    {
        return (!$user->isPasswordLegal()) ? 'Passwords do not match!' : '';
    }

}