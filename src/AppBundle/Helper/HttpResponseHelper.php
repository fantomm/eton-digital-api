<?php
namespace AppBundle\Helper;


class HttpResponseHelper
{
    /**
     * Format success response
     * @param array $data
     * @param string $status
     * @param int $httpCode
     * @return array
     */
    public static function successResponse(array $data, $status = 'success', $httpCode = 200)
    {
        return [
            'status'    => $status,
            'data'      => $data,
            'httpCode'  => $httpCode
        ];
    }

    /**
     * Format Error Response
     * @param string $message
     * @param $httpCode
     * @return array
     */
    public static function errorResponse($message = '', $httpCode)
    {
        return [
            'status'    => 'error',
            'message'   => $message,
            'httpCode'  => $httpCode
        ];
    }

}