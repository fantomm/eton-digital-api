<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Helper\ValidationHelper as Validation;
use AppBundle\Helper\HttpResponseHelper as HttpResponse;

class ApiController extends Controller
{
    /**
     * Register New User
     * @param Request $request
     * @return JsonResponse
     */
    public function registerAction(Request $request)
    {
        // Get JSON data
        $body     = $request->getContent();
        $formData = json_decode($body, true);

        // Get data from form-data (array)
        // $body     = $request->request->all();
        // $formData = $body;

        // Validate request
        if(!Validation::isJSON($body)) {
            $data = HttpResponse::errorResponse(sprintf('Submitted data format is not valid! Json expected!'), Response::HTTP_BAD_REQUEST);
            return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
        }

        // Prepare Entity
        $user = $this->generateUser($formData);

        // Validate Entity
        $errorsResponse = '';
        $errorsResponse .= Validation::validateEntity($user, $this->container);
        $errorsResponse .= Validation::checkPasswordConfirmation($user);

        if(!empty($errorsResponse)) {
            $data = HttpResponse::errorResponse(sprintf($errorsResponse), Response::HTTP_UNPROCESSABLE_ENTITY);
            return new JsonResponse($data, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        // Save Entity
        $response = $this->saveEntity($user);

        if($response)
        {
            // Send Email Confirmation Token:
            $this->sendEmailConfirmationToken($user);

            // Return response Entity and http code
            $data = HttpResponse::successResponse($response->normalized(), 'success', Response::HTTP_CREATED);
            return new JsonResponse($data, Response::HTTP_CREATED);
        }

        // Entity not saved
        $data = HttpResponse::errorResponse(sprintf('Submitted data is not valid!'), Response::HTTP_BAD_REQUEST);
        return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
    }


    /**
     * Confirm/Enable User
     * @param Request $request
     * @param string $username
     * @return JsonResponse
     */
    public function confirmAction(Request $request, $username = '')
    {
        // Get JSON data
        $body     = $request->getContent();
        $formData = json_decode($body, true);
        $confirmationToken = (isset($formData['confirmation_token'])) ? $formData['confirmation_token'] : '';
        // $username = (null !== $request->get('username')) ? $request->get('username') : '';
        $username = (!empty($username)) ? $username : '';

        // Get data from form-data (array)
        // $body     = $request->request->all();
        // $formData = $body;

        // Validate request
        if(!Validation::isJSON($body)) {
            $data = HttpResponse::errorResponse(sprintf('Submitted data format is not valid! Json expected!'), Response::HTTP_BAD_REQUEST);
            return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
        }

        // Get Entity
        $user = $this->getOneUserBy($username, $confirmationToken);

        // Validate
        if(!$user)
        {
            $data = HttpResponse::errorResponse(sprintf('Invalid User and/or Confirmation Token supplied!'), Response::HTTP_NOT_FOUND);
            return new JsonResponse($data, Response::HTTP_NOT_FOUND);
        }
        elseif($user->getStatus() == User::STATUS_CONFIRMED)
        {
            $data = HttpResponse::errorResponse(sprintf('Conflict! Token already confirmed'), Response::HTTP_CONFLICT);
            return new JsonResponse($data, Response::HTTP_CONFLICT);
        }

        // Confirm/Enable User
        $response = $this->confirmUser($user);

        // Return response Entity and http code
        if($response) {
            $data = HttpResponse::successResponse($response->normalized(), 'success', Response::HTTP_OK);
            return new JsonResponse($data, Response::HTTP_OK);
        }

        // Entity not changed
        $data = HttpResponse::errorResponse(sprintf('User NOT confirmed!'), Response::HTTP_BAD_REQUEST);
        return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
    }


    /**
     * Generate a user
     * @param $formData
     * @return User
     */
    public function generateUser($formData)
    {
        $user = new User();
        $user->setEmail   (isset($formData['email'])    ? $formData['email']    : '');
        $user->setUsername(isset($formData['username']) ? $formData['username'] : '');
        $user->setPassword(isset($formData['password']) ? $formData['password'] : '');
        $user->setConfirmPassword(isset($formData['confirmationPassword']) ? $formData['confirmationPassword'] : '');
        return $user;
    }


    /**
     * Get User Repository
     * @return \AppBundle\Model\UserRepository|\Doctrine\Common\Persistence\ObjectRepository
     */
    public function getUserRepository()
    {
        return $this->getDoctrine()->getRepository('AppBundle:User');
    }


    /**
     * @param $user
     * @return bool
     */
    public function saveEntity($user)
    {
        $em = $this->getUserRepository();
        $response = $em->save($user);
        return $response;
    }


    /**
     * Get one user by username and token
     * @param $username
     * @param $confirmationToken
     * @return User|null|object
     */
    public function getOneUserBy($username, $confirmationToken)
    {
        $em = $this->getUserRepository();
        $user = $em->findOneBy(array('username' => $username, 'confirmation_token' => $confirmationToken));
        return $user;
    }


    /**
     * Confirm User
     * @param $user
     * @return mixed
     */
    public function confirmUser($user)
    {
        $user->setStatus(User::STATUS_CONFIRMED);

        $em = $this->getUserRepository();
        $response = $em->save($user);
        return $response;
    }


    /**
     * Simple Php email sending:
     * ( NOTE: Email could be sent using a "listener/subscriber" to
     * listen when a new user record gets added to the User table ).
     *
     * @param User $user
     */
    public function sendEmailConfirmationToken(User $user)
    {
        // mail ( $user->getEmail() , 'Confirm you account' , 'Please confirm your account by clicking <a href="..../confirmation_page/'.$user->getUsername().'/'.$user->getConfirmationToken().'" > here </a>' );
    }

}