<?php

namespace AppBundle\Model;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    public function save($entity)
    {
        $em = $this->getEntityManager();

        $em->persist($entity);
        try {
            $em->flush();
        } catch (\Exception $ex) {
            // echo $ex->getMessage();
            return false;
        }
        return $entity;
    }

}