<?php
namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @UniqueEntity("username", message="Username is already taken!")
 * @UniqueEntity("email", message="Email is already taken!")
 * @package AppBundle\Entity
 */
class User
{
    const HASH_ALGORITHM        = 'sha1';
    const STATUS_CONFIRMED      = 1;
    const STATUS_NOT_CONFIRMED  = 0;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
        protected $id;

    /**
     * @Assert\NotBlank(message="Username should not be blank!")
     * @ORM\Column(type="string", length=40)
     */
        protected $username;

    /**
     * @Assert\NotBlank(message="Email should not be blank!")
     * @Assert\Email(message="Email address format is invalid!")
     * @ORM\Column(type="string", length=50)
     */
        protected $email;

    /**
     * @Assert\NotBlank(message="Password should not be blank!")
     * @ORM\Column(type="string", length=40)
     */
        protected $password;

    /**
     * @Assert\NotBlank(message="Confirm Password should not be blank!")
     */
        protected $confirmPassword;

    /**
     * @Assert\NotBlank(message="Confirmation token should not be blank!")
     * @ORM\Column(type="string", length=40)
     */
        protected $confirmation_token;

    /**
     * @Assert\NotBlank(message="Status should not be blank!")
     * @ORM\Column(type="integer")
     */
        protected $status = self::STATUS_NOT_CONFIRMED;


    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->confirmation_token = $this->generateToken();
    }

    /**
     * Generate Confirmation Token
     * @return string
     */
        public function generateToken()
        {
            $cStrong = True;
            return bin2hex(openssl_random_pseudo_bytes(20, $cStrong));
        }

    /**
     * Get normalized representation of the class
     * @return array
     */
        public function normalized()
        {
            return array(
                'id'                    => $this->getId(),
                'email'                 => $this->getEmail(),
                'username'              => $this->getUsername(),
                'password'              => $this->getPassword(),
                'confirmation_token'    => $this->getConfirmationToken(),
                'status'                => $this->getStatus()
            );
        }


    /**
     * Check if passwords match
     * @return bool
     */
    public function isPasswordLegal()
    {
        return ($this->password == $this->confirmPassword);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password =  hash(self::HASH_ALGORITHM, $password);
    }

    /**
     * @return mixed
     */
    public function getConfirmPassword()
    {
        return $this->confirmPassword;
    }

    /**
     * @param mixed $confirmPassword
     */
    public function setConfirmPassword($confirmPassword)
    {
        $this->confirmPassword = hash(self::HASH_ALGORITHM, $confirmPassword);
    }

    /**
     * @return mixed
     */
    public function getConfirmationToken()
    {
        return $this->confirmation_token;
    }

    /**
     * @param mixed $confirmation_token
     */
    public function setConfirmationToken($confirmation_token)
    {
        $this->confirmation_token = $confirmation_token;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatusConfirmed()
    {
        return self::STATUS_CONFIRMED;
    }

    /**
     * @return int
     */
    public function getStatusNotConfirmed()
    {
        return self::STATUS_NOT_CONFIRMED;
    }

}