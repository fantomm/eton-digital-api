Eton Digital Test Project
=========================

Requirements:
--------------
  * Implement API service which will be used to register and confirm/enable registered users.

  * Service will have 2 resources:
    * '**/api/v1/users**' - this resource will be used for registering a user by sending user data _(username, email, password, confirm_password)_ and sending a confirmation token to the users email address. Resource should return a user entity in the response. 
    * '**/api/v1/users/confirm**' - this resource will be used to confirm/enable a user by sending the confirmation token via patch method. Resource should return an updated user entity in the response.

  * It would be desirable if both resources contained validation and returned errors in case of incorrect data.

  * Feel free to use the existing bundles (FOSUser, FOSRest, ...) for the user, rest service, and the rest that you need so that you do not need to write everything.

  * When you find time to implement and complete the task, send us a solution or link to the repository where you worked.
  
  
Setup Project:
--------------
 * Clone repository to your local machine:
    * $ git clone https://fantomm@bitbucket.org/fantomm/eton-digital-api.git
 * Update dependencies (create vendor dir):
    * $ composer update
 * Create and update database:
    * $ php app/console doctrine:database:create
    * $ php app/console doctrine:schema:update --dump-sql
    * $ php app/console doctrine:schema:update --force
    
  
How to use?:
------------
 * Using [Postman](https://www.getpostman.com/) to test API.
 * Expected input data format: _JSON_.


Examples:
---------
 * Register user:
     * Link: http://[path_to_project]/api/v1/users
     * Method: [_POST_]
     * Body (raw):  
        ```
        {
            "username"              : "milan",
            "email"                 : "milan@gmail.com",
            "password"              : "pass1",
            "confirmationPassword"  : "pass1"
        }
        ```
     * Possible return after successful registration:  
        ```
        {
            "status" : "success",
            "data"   : {
                            "id"        : 43,
                            "email"     : "milan@gmail.com",
                            "username"  : "milan",
                            "password"  : "f0578f1e7174b1a41c4ea8c6e17f7a8a3b88c92a",
                            "confirmation_token": "7c19defad4a263a3039c4605e5e4374f6f28a409",
                            "status"    : 0
                        },
            "httpCode": 201
        }
        ```
     * Possible return after un-successful registration:  
         ```
         {
            "status"    : "error",
            "message"   : "Email is already taken! Username should not be blank!",
            "httpCode"  : 422
         }
         ```
  * Confirm/Enable user:
      * Link: http://[path_to_project]/api/v1/users/confirm/milan
      * Method: [_PATCH_]
      * Body (raw):  
         ```
         {
             "confirmation_token" : "7c19defad4a263a3039c4605e5e4374f6f28a409"
         }
         ```
      * Possible return after successful confirmation:  
            ```
             {
                 "status" : "success",
                 "data"   : {
                                 "id"        : 43,
                                 "email"     : "milan@gmail.com",
                                 "username"  : "milan",
                                 "password"  : "f0578f1e7174b1a41c4ea8c6e17f7a8a3b88c92a",
                                 "confirmation_token": "7c19defad4a263a3039c4605e5e4374f6f28a409",
                                 "status"    : 1
                             },
                 "httpCode": 200
             }
            ```
      * Possible return after un-successful confirmation:  
            ```
            {
              "status"    : "error",
              "message"   : "Conflict! Token already confirmed",
              "httpCode"  : 409
            }
            ```
         
Other info:
-----------
 * Project is done in Symfony 2.8.26.
 * No third-party bundles used.
 * Third-party bundles that could have been used:
    * _JMSSerializerBundle_ - to serialize an entity (when returning entity in response).
    * _Swift Mailer_
    * _FOSUser bundle_
    * _REST bundle_
    
Enjoy.